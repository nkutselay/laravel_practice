<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Vote;

class PostController extends Controller
{
    /**
     * Return page with post by id
     *
     * @param  Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $post->load([
                'author',
                'hubs',
                'marks',
                'votes'
            ])->loadCount([
                'comments',
                'bookmarks'
            ]);

        return response()->view('post.index', [ 'post' => $post ]);
    }

    /**
     * Add vote to the post
     *
     * @param Post $post
     * @return \Illuminate\Http\Response
     */
    public function vote(Post $post)
    {
        $post = \DB::transaction(function () use ($post) {
            $newVoteValue = request()->input('data.value');
            $userVotes = request()->session()->get('votes', []);

            if ($post->alreadyVotedByUser($userVotes)) {
                abort(403, 'Post already voted');
            }

            $newVote = $post->votes()->save(new Vote([ 'value' => $newVoteValue ]));
            $post->increment('rating', $newVoteValue);

            request()->session()->push('votes', $newVote->id);

            $post->loadCount(['positiveVotes', 'negativeVotes']);

            return $post;
        });

        return response()->json([
            'positiveVotesCount' => $post->positive_votes_count,
            'negativeVotesCount' => $post->negative_votes_count
        ]);
    }
}
