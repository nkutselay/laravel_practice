<?php

namespace App\Http\Controllers;

use App\Models\Hub;
use App\Models\Post;

class HubController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Hub  $hub
     * @param string $top
     * @return \Illuminate\Http\Response
     */
    public function show(Hub $hub, string $top = null)
    {
        $posts = Post::whereHas('hubs', fn($query) => $query->where('hubs.id', $hub->id))
            ->top($top)
            ->with([
                'author',
                'hubs',
                'marks'
            ])
            ->withCount([
                'comments',
                'bookmarks',
                'votes',
                'positiveVotes',
                'negativeVotes'
            ])
            ->paginate(10);

        return response()->view('hub.index', compact('hub', 'posts', 'top'));
    }
}
