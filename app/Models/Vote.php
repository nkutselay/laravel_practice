<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $fillable = ['value'];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
