<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Post extends Model
{
    const TOP_PREFIX = 'top';

    public function getRouteKeyName()
    {
        return 'post';
    }

    public function resolveRouteBinding($value)
    {
        return $this->findOrFail($value);
    }

    public function scopeTop($query, $top)
    {
        if ($top && Str::startsWith($top, self::TOP_PREFIX)) {
            $rating = (int) Str::substr($top, strlen(self::TOP_PREFIX));
            return $query->where('rating', '>=', $rating);
        }

        return $query;
    }

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    public function hubs()
    {
        return $this->belongsToMany(hub::class);
    }

    public function marks()
    {
        return $this->belongsToMany(Mark::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function bookmarks()
    {
        return $this->hasMany(Bookmark::class);
    }

    public function votes()
    {
        return $this->hasMany(Vote::class);
    }

    public function positiveVotes()
    {
        return $this->votes()->where('value', 1);
    }

    public function negativeVotes()
    {
        return $this->votes()->where('value', -1);
    }

    public function getPreviewAttribute()
    {
        return implode('. ', array_slice(explode('. ', $this->content), 0, 7));
    }

    public function getCreatedDateAttribute()
    {
        if ($this->created_at->isToday()) {
            return $this->created_at->translatedFormat('сегодня в H:i');
        } else if ($this->created_at->isYesterday()) {
            return $this->created_at->translatedFormat('вчера в H:i');
        } else {
            return $this->created_at->translatedFormat('j F Y в H:i');
        }
    }

    public function getPositiveVotes() {
        return $this->votes->filter(function ($vote, $key){
                return $vote->value === 1;
            });
    }

    public function getNegativeVotes() {
        return $this->votes->filter(function ($vote, $key){
                return $vote->value === -1;
            });
    }

    public function alreadyVotedByUser($userVotes) {
        foreach ($userVotes as $userVote) {
            $postVote = $this->votes->first(function ($postVote, $key) use ($userVote) {
                return $postVote->id === $userVote;
            });

            if ($postVote) {
                return true;
            }
        }
        return false;
    }
}
