<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hub extends Model
{
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function resolveRouteBinding($value)
    {
        return $this->where('slug', $value)->firstOrFail();
    }

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}
