<?php

use Illuminate\Database\Seeder;

class HubsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Hub::class, 6)->create();
    }
}
