<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    $users = User::all();
    $posts = Post::all();

    return [
        'author_id' => $users->random(1)->first()->id,
        'post_id' => $posts->random(1)->first()->id,
        'text' => $faker->realText(200)
    ];
});
