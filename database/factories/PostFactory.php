<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Hub;
use App\Models\Mark;
use App\Models\User;
use App\Models\Post;
use App\Models\Tag;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    $users = User::all();

    return [
        'author_id' => $users->random(1)->first()->id,
        'title' => mb_substr($faker->sentence($faker->numberBetween(5, 10), true), 0, -1),
        'content' => $faker->realText(5000),
        'views' => $faker->numberBetween(1, 50)
    ];
});

$factory->afterCreating(Post::class, function(Post $post, Faker $faker) {
    $hubs = Hub::all();
    $tags = Tag::all();

    $post->hubs()->attach(
        $hubs->random(rand(1, $hubs->count()))->pluck('id')->toArray()
    );

    $post->tags()->attach(
        $tags->random(rand(1, $tags->count()))->pluck('id')->toArray()
    );

    $needSetMarks = $faker->numberBetween(1, 5) === 1;

    if ($needSetMarks) {
        $marks = Mark::all();
        $post->marks()->attach(
            $marks->random(rand(1, $marks->count()))->pluck('id')->toArray()
        );
    }
});
