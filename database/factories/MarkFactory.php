<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Mark;
use Faker\Generator as Faker;

$factory->define(Mark::class, function (Faker $faker) {
    $title = $faker->unique()->randomElement(['Из Песочницы', 'Tutorial', 'Интересно']);

    return [
        'title' => $title
    ];
});
