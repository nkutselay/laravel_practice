<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Hub;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

use Ottaviano\Faker\Gravatar;

$factory->define(Hub::class, function (Faker $faker) {
    $faker->addProvider(new Gravatar($faker));

    $title = $faker->unique()->randomElement(['PHP', 'Программирование', 'Laravel', 'NodeJS', 'Symfony', 'Javascript']);

    return [
        'title' => $title,
        'slug' => Str::slug($title, '_'),
        'image' => $faker->gravatarUrl('identicon', null, 200),
        'description' => $faker->sentence(7, true)
    ];
});
