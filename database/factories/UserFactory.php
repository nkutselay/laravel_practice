<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\User;
use Faker\Generator as Faker;
use Ottaviano\Faker\Gravatar;

$factory->define(User::class, function (Faker $faker) {
    $faker->addProvider(new Gravatar($faker));

    $email = $faker->unique()->safeEmail;

    return [
        'nickname' => $faker->unique()->userName,
        'email' => $faker->unique()->safeEmail,
        'full_name' => $faker->name(),
        'avatar' => $faker->gravatarUrl('retro', $email, 200)
    ];
});
