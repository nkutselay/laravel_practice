<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Post;
use App\Models\User;
use App\Models\Vote;
use Faker\Generator as Faker;

$factory->define(Vote::class, function (Faker $faker) {
    $posts = Post::all();

    return [
        'post_id' => $posts->random(1)->first()->id,
        'value' => $faker->numberBetween(1, 5) !== 1 ? 1 : -1
    ];
});

$factory->afterCreating(Vote::class, function(Vote $vote, Faker $faker) {
    $post = Post::find($vote->post_id);
    $post->increment('rating', $vote->value);
});
