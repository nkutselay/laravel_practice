<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Tag;
use Faker\Generator as Faker;

$factory->define(Tag::class, function (Faker $faker) {
    $title = $faker->unique()->randomElement([
        'PHP', 'laravel', 'symfony',
        'Node.js', 'javascript', 'logging',
        'debug', 'deno', 'typescript'
    ]);

    return [
        'title' => $title,
        'slug' => Str::slug($title, '_'),
    ];
});
