<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/webpack', function () {
    return view('webpack_test');
});

Route::get('robots.txt', 'RobotsTxtController');

Route::get('/hub/{hub}/{top?}', 'HubController@show')->name('hub');

Route::get('/post/{post}', 'PostController@show')->name('post');
Route::post('/post/{post}/vote', 'PostController@vote');
