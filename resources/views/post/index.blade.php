@extends('layouts.main')

@section('content')
<div class="layout__cell layout__cell_body">
    <section class="column-wrapper column-wrapper_post js-sticky-wrapper">
        <div class="content_left js-content_left">
            <article class="post post_full" id="post_{{ $post->id }}" lang="ru">
                <div class="post__wrapper">
                    <header class="post__meta">
                        <a href="#" class="post__user-info user-info" title="Автор публикации">
                            <img src="{{ $post->author->avatar }}" width="24" height="24" class="user-info__image-pic user-info__image-pic_small">
                            <span class="user-info__nickname user-info__nickname_small">{{ $post->author->nickname }}</span>
                        </a>
                        <span class="post__time" data-time_published="{{ $post->created_at }}">{{ $post->created_date }}</span>
                    </header>

                    <h1 class="post__title post__title_full">
                        <span class="post__title-text">{{ $post->title }}</span>
                    </h1>

                    <ul class="post__hubs post__hubs_full-post inline-list">
                        @foreach ($post->hubs as $hub)
                        <li class="inline-list__item inline-list__item_hub">
                            <a href="{{ route('hub', [ 'hub' => $hub->slug ]) }}"
                                class="inline-list__item-link hub-link "
                                rel="nofollow"
                                title="Вы не подписаны на этот хаб">
                                {{ $hub->title }},
                            </a>
                        </li>
                        @endforeach
                    </ul>

                    <ul class="post__marks inline-list">
                        @foreach ($post->marks as $mark)
                        <li class="inline-list__item inline-list__item_post-type">
                            <a href="#" class="post__type-label" title="Перейти в песочницу">{{ $mark->title }}</a>
                        </li>
                        @endforeach
                    </ul>

                    <div class="post__body post__body_full">
                        <div class="post__text post__text-html post__text_v1" id="post-content-body" data-io-article-url="{{ route('post', [ 'post' => $post->id ]) }}">
                            {{ $post->content }}
                        </div>
                    </div>

                    <dl class="post__tags">
                        <dt class="post__tags-label">Теги:</dt>
                        <dd class="post__tags-list">
                            <ul class="inline-list inline-list_fav-tags js-post-tags">
                                @foreach ($post->tags as $tag)
                                <li class="inline-list__item inline-list__item_tag">
                                    <a href="#" rel="tag" class="inline-list__item-link post__tag  ">
                                        {{ $tag->title }}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </dd>
                    </dl>

                    <dl class="post__tags">
                        <dt class="post__tags-label">Хабы:</dt>
                        <dd class="post__tags-list">
                            <ul class="inline-list inline-list_fav-tags js-post-hubs">
                                @foreach ($post->hubs as $hub)
                                <li class="inline-list__item inline-list__item_tag">
                                    <a href="{{ route('hub', [ 'hub' => $hub->slug ]) }}" rel="tag" class="inline-list__item-link post__tag  ">
                                        {{ $hub->title }}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </dd>
                    </dl>
                </div>
            </article>
        </div>
    </section>

    <section class="column-wrapper column-wrapper_post-additionals">
        <div class="content_left">
            <div class="post-additionals">
                <ul class="post-stats post-stats_post js-user_" data-post-type="publish_ugc_ru,h_91,h_17110,f_develop" id="infopanel_post_{{ $post->id }}">
                    <li class="post-stats__item post-stats__item_voting-wjt">
                        <post-votes
                            :post-id="{{ $post->id }}"
                            :already-voted="{{ $post->alreadyVotedByUser(session('votes', [])) ? 'true' : 'false' }}"
                            :positive-votes="{{ $post->getPositiveVotes()->count() }}"
                            :negative-votes="{{ $post->getNegativeVotes()->count() }}">
                        </post-votes>
                    </li>

                    <li class="post-stats__item post-stats__item_bookmark">
                        <button type="button" class="btn bookmark-btn bookmark-btn_post " data-post-type="publish_ugc_ru,h_91,h_17110,f_develop" data-type="2" data-id="{{ $post->id }}" data-action="add" title="Только зарегистрированные пользователи могут добавлять публикации в закладки" onclick="posts_add_to_favorite(this);" disabled="">
                            <span class="btn_inner">
                                <svg class="icon-svg_bookmark" width="10" height="16">
                                    <use xlink:href="/images/svg/common-svg-sprite.svg#book"></use>
                                </svg>
                                <span class="bookmark__counter js-favs_count" title="Количество пользователей, добавивших публикацию в закладки">
                                    {{ $post->bookmarks_count }}
                                </span>
                            </span>
                        </button>
                    </li>

                    <li class="post-stats__item post-stats__item_views">
                        <div class="post-stats__views" title="Количество просмотров">
                            <svg class="icon-svg_views-count" width="21" height="12">
                                <use xlink:href="/images/svg/common-svg-sprite.svg#eye"></use>
                            </svg>
                            <span class="post-stats__views-count">
                                {{ $post->views }}
                            </span>
                        </div>
                    </li>

                    <li class="post-stats__item post-stats__item_comments">
                        <a href="{{ route('post', [ 'post' => $post->id ]) }}/#comments" class="post-stats__comments-link" rel="nofollow">
                            <svg class="icon-svg_post-comments" width="16" height="16">
                                <use xlink:href="/images/svg/common-svg-sprite.svg#comment"></use>
                            </svg>
                            <span class="post-stats__comments-count" title="Читать комментарии">
                                {{ $post->comments_count }}
                            </span>
                        </a>
                    </li>

                    <li class="post-stats__item post-stats__item_share">
                        <div class="dropdown dropdown_share">
                            <div href="{{ route('post', [ 'post' => $post->id ]) }}/#comments" data-toggle="dropdown" class="post-stats__share" rel="nofollow" tabindex="0">
                                <svg class="icon-svg_post-share" width="24" height="24">
                                    <use xlink:href="/images/svg/common-svg-sprite.svg#share"></use>
                                </svg>
                                <span class="post-stats__comments-text" title="Поделиться">
                                    Поделиться
                                </span>
                            </div>
                            <div class="dropdown-container">
                                <div class="post-share">
                                    <ul class="post-share__buttons">
                                        <li class="post-share__item post-share__item_post">
                                            <span class="post-share__item-link post-share__item-link_normal post-share__item-link_copy" title="Скопировать ссылку" onclick="copyCurrentUrl(), $('.dropdown_share').removeClass('dropdown_active')">
                                                Скопировать ссылку
                                            </span>
                                        </li>
                                        <li class="post-share__item post-share__item_post">
                                            <a href="https://www.facebook.com/sharer/sharer.php?u=https://habr.com/ru/post/{{ $post->id }}/" class="post-share__item-link post-share__item-link_normal post-share__item-link_facebook" title="Facebook" onclick="window.open(this.href, 'Facebook', 'width=640,height=436,toolbar=0,status=0'); return false">
                                                Facebook
                                            </a>
                                        </li>
                                        <li class="post-share__item post-share__item_post">
                                            <a href="https://twitter.com/intent/tweet?text=5+%D0%BF%D1%80%D0%B5%D1%82%D0%B5%D0%BD%D0%B7%D0%B8%D0%B9+%D0%BA+Deno+https://habr.com/p/{{ $post->id }}/+via+%40habr_com" class="post-share__item-link post-share__item-link_normal post-share__item-link_twitter" title="Twitter" onclick="window.open(this.href, 'Twitter', 'width=800,height=300,resizable=yes,toolbar=0,status=0'); return false">
                                                Twitter
                                            </a>
                                        </li>
                                        <li class="post-share__item post-share__item_post">
                                            <a href="https://vk.com/share.php?url=https://habr.com/ru/post/{{ $post->id }}/" class="post-share__item-link post-share__item-link_normal post-share__item-link_vkontakte" title="ВКонтакте" onclick="window.open(this.href, 'ВКонтакте', 'width=800,height=300,toolbar=0,status=0'); return false">
                                                ВКонтакте
                                            </a>
                                        </li>
                                        <li class="post-share__item post-share__item_post">
                                            <a href="https://t.me/share/url?url=https://habr.com/ru/post/504964/&amp;title=5 претензий к Deno" class="post-share__item-link post-share__item-link_normal post-share__item-link_telegram" title="Telegram" onclick="window.open(this.href, 'Telegram', 'width=800,height=300,toolbar=0,status=0'); return false">
                                                Telegram
                                            </a>
                                        </li>
                                        <li class="post-share__item post-share__item_post">
                                            <a href="https://getpocket.com/edit?url=https://habr.com/ru/post/504964/&amp;title=5 претензий к Deno" class="post-share__item-link post-share__item-link_normal post-share__item-link_pocket" title="Pocket" target="_blank" rel=" noopener">
                                                Pocket
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>

                <div class="author-panel author-panel_user">
                    <div class="author-panel__user-info">
                        <div class="user-info" data-user-login="${{ $post->author->nickname }}">
                            <a href="#" class="media-obj__image">
                                <img src="{{ $post->author->avatar }}" width="48" height="48" class="media-obj__image-pic media-obj__image-pic_user">
                            </a>

                            <div class="user-info__about">
                                <div class="user-info__links">
                                    <a href="#" class="user-info__nickname user-info__nickname_doggy">
                                        {{ $post->author->nickname }}
                                    </a>
                                </div>
                            <div class="user-info__specialization">JavaScript разработчик</div>
                        </div>
                    </div>
                </div>

                <ul class="user-info__contacts inline-list">
                    <li class="inline-list__item inline-list__item_contact-link"><a href="https://qna.habr.com/user/{{ $post->author->nickname }}">Хабр Q&amp;A</a></li>
                    <li class="inline-list__item inline-list__item_contact-link"><a href="https://github.com/{{ $post->author->nickname }}/">Github</a></li>
                    <li class="inline-list__item inline-list__item_contact-link"><a href="https://telegram.me/{{ $post->author->nickname }}">Telegram</a></li>
                </ul>
            </div>
        </div>
    </section>
</div>
@endsection
