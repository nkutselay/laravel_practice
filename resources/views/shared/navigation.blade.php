<div class="layout__row layout__row_navbar">
    <div class="layout__cell">
        <div class="main-navbar">
            <div class="main-navbar__section main-navbar__section_left">
                <ul class="nav-links" id="navbar-links">
                    <li class="nav-links__item">
                        <a href="#" class="nav-links__item-link ">Все потоки</a>
                    </li>
                    <li class="nav-links__item">
                        <a href="#" class="nav-links__item-link ">Разработка</a>
                    </li>
                    <li class="nav-links__item">
                        <a href="#" class="nav-links__item-link ">Администрирование</a>
                    </li>
                    <li class="nav-links__item">
                        <a href="#" class="nav-links__item-link ">Дизайн</a>
                    </li>
                    <li class="nav-links__item">
                        <a href="#" class="nav-links__item-link ">Менеджмент</a>
                    </li>
                    <li class="nav-links__item">
                        <a href="#" class="nav-links__item-link ">Маркетинг</a>
                    </li>
                    <li class="nav-links__item">
                        <a href="#" class="nav-links__item-link ">Научпоп</a>
                    </li>
                </ul>
                <form action="#" method="get" class="search-form" id="search-form">
                    <button type="button" class="btn btn_navbar_search icon-svg_search" id="search-form-btn" title="Поиск по сайту">
                        <svg class="icon-svg" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M17 11C17 14.3137 14.3137 17 11 17C7.68629 17 5 14.3137 5 11C5 7.68629 7.68629 5 11 5C14.3137 5 17 7.68629 17 11ZM15.5838 17.5574C14.2857 18.4665 12.7051 19 11 19C6.58172 19 3 15.4183 3 11C3 6.58172 6.58172 3 11 3C15.4183 3 19 6.58172 19 11C19 12.9998 18.2662 14.8282 17.0533 16.2307C17.0767 16.2502 17.0994 16.2709 17.1214 16.2929L20.4143 19.5858C20.8048 19.9763 20.8048 20.6095 20.4143 21C20.0238 21.3905 19.3906 21.3905 19.0001 21L15.7072 17.7071C15.6605 17.6604 15.6194 17.6102 15.5838 17.5574Z"></path>
                        </svg>
                    </button>
                    <label class="search-form__field-wrapper">
                        <input type="text" name="q" class="search-form__field" id="search-form-field" placeholder="Поиск" tabindex="-1">
                        <button type="button" class="btn btn_search-close" id="search-form-clear" title="Закрыть">
                            <svg class="icon-svg icon-svg_navbar-close-search" width="31" height="32" viewBox="0 0 31 32" aria-hidden="true" version="1.1" role="img">
                                <path d="M26.67 0L15.217 11.448 3.77 0 0 3.77l11.447 11.45L0 26.666l3.77 3.77L15.218 18.99l11.45 11.448 3.772-3.77-11.448-11.45L30.44 3.772z"></path>
                            </svg>
                        </button>
                    </label>
                </form>
            </div>

            <div class="main-navbar__section main-navbar__section_right">
                <button type="button" class="btn btn_medium btn_navbar_lang js-show_lang_settings">
                    <svg class="icon-svg" width="18" height="18">
                        <use xlink:href="/images/svg/common-svg-sprite.svg#globus-v2"></use>
                    </svg>
                </button>
                <a href="#" id="login" class="btn btn_medium btn_navbar_login">Войти</a>
                <a href="#" class="btn btn_medium btn_navbar_registration">Регистрация</a>
            </div>
        </div>
    </div>
</div>
