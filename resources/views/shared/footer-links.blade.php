<div class="layout__row layout__row_footer-links">
    <div class="layout__cell">
        <div class="footer-grid footer-grid_menu">

            <div class="footer-grid__item footer-block">
                <h3 class="footer-block__title">Ваш аккаунт</h3>
                <div class="footer-block__content">
                    <ul class="footer-menu">
                        <li class="footer-menu__item">
                            <a href="#" class="footer-menu__item-link">Войти</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="#" class="footer-menu__item-link">Регистрация</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="footer-grid__item footer-block">
                <h3 class="footer-block__title">Разделы</h3>
                <div class="footer-block__content">
                    <ul class="footer-menu">
                        <li class="footer-menu__item">
                            <a href="#" class="footer-menu__item-link">Публикации</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="#" class="footer-menu__item-link">Новости</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="#" class="footer-menu__item-link">Хабы</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="#" class="footer-menu__item-link">Компании</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="#" class="footer-menu__item-link">Пользователи</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="#" class="footer-menu__item-link">Песочница</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="footer-grid__item footer-block">
                <h3 class="footer-block__title">Информация</h3>
                <div class="footer-block__content">
                    <ul class="footer-menu">
                        <li class="footer-menu__item">
                            <a href="#" class="footer-menu__item-link">Устройство сайта</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="#" class="footer-menu__item-link">Для авторов</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="#" class="footer-menu__item-link">Для компаний</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="#" class="footer-menu__item-link">Документы</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="#" class="footer-menu__item-link">Соглашение</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="#" class="footer-menu__item-link">Конфиденциальность</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="footer-grid__item footer-block">
                <h3 class="footer-block__title">Услуги</h3>
                <div class="footer-block__content">
                    <ul class="footer-menu">
                        <li class="footer-menu__item">
                            <a href="#" target="_blank" class="footer-menu__item-link" rel=" noopener">Реклама</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="#" target="_blank" class="footer-menu__item-link" rel=" noopener">Тарифы</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="#" target="_blank" class="footer-menu__item-link" rel=" noopener">Контент</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="#" target="_blank" class="footer-menu__item-link" rel=" noopener">Семинары</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="#" target="_blank" class="footer-menu__item-link" rel=" noopener">Мегапроекты</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="footer-misprints">
            Если нашли опечатку в&nbsp;посте, выделите ее&nbsp;и&nbsp;нажмите Ctrl+Enter, чтобы сообщить автору.
        </div>
    </div>
</div>
