<div class="layout__cell layout__cell_body">
    <div class="page-header page-header_full" id="hub_{{ $hub->id }}">
        <div class="page-header_wrapper">
            <div class="media-obj media-obj_page-header">
                <a href="{{ route('hub', [ 'hub' => $hub->slug ]) }}" class="media-obj__image">
                    <img src="{{ $hub->image }}" width="48" height="48" class="media-obj__image-pic">
                </a>
                <div class="media-obj__body media-obj__body_page-header media-obj__body_page-header_hub">
                <div class="page-header__stats">
                    <div class="page-header__stats-value">111,92</div>
                    <div class="page-header__stats-label">Рейтинг</div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-header__info">
        <h1 class="page-header__info-title">{{ $hub->title }}</h1>
        <span class="n-profiled_hub" title="Профильный хаб"></span>
        <h2 class="page-header__info-desc">{{ $hub->description }}</h2>
    </div>
</div>
