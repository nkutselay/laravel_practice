<div class="tabs">
    <div class="tabs__level tabs-level_top tabs-menu">
        <a href="#" class="tabs-menu__item tabs-menu__item_link" rel="nofollow">
            <h3 class="tabs-menu__item-text tabs-menu__item-text_active">Все подряд</h3>
        </a>
        <a href="#" class="tabs-menu__item tabs-menu__item_link" rel="nofollow">
            <h3 class="tabs-menu__item-text ">Лучшие</h3>
        </a>
        <a href="#" class="tabs-menu__item tabs-menu__item_link" rel="nofollow">
            <h3 class="tabs-menu__item-text ">Авторы</h3>
        </a>
    </div>
    <div class="tabs__level tabs__level_bottom">
        <ul class="toggle-menu ">
            <li class="toggle-menu__item">
                <a href="{{ route('hub', [ 'hub' => $hub->slug, 'top' => 'all' ]) }}"
                    class="toggle-menu__item-link {{ $top === null || $top === 'all' ? 'toggle-menu__item-link_active' : '' }}"
                    rel="nofollow"
                    title="Все публикации в хронологическом порядке">
                    Без порога
                </a>
            </li>
            <li class="toggle-menu__item">
                <a href="{{ route('hub', [ 'hub' => $hub->slug, 'top' => 'top10' ]) }}"
                    class="toggle-menu__item-link {{ $top === 'top10' ? 'toggle-menu__item-link_active' : '' }}"
                    rel="nofollow"
                    title="Все публикации с рейтингом 10 и выше">
                    ≥10
                </a>
            </li>
            <li class="toggle-menu__item">
                <a href="{{ route('hub', [ 'hub' => $hub->slug, 'top' => 'top25' ]) }}"
                    class="toggle-menu__item-link {{ $top === 'top25' ? 'toggle-menu__item-link_active' : '' }}"
                    rel="nofollow"
                    title="Все публикации с рейтингом 25 и выше">
                    ≥25
                </a>
            </li>
            <li class="toggle-menu__item">
                <a href="{{ route('hub', [ 'hub' => $hub->slug, 'top' => 'top50' ]) }}"
                    class="toggle-menu__item-link {{ $top === 'top50' ? 'toggle-menu__item-link_active' : '' }}"
                    rel="nofollow"
                    title="Все публикации с рейтингом 50 и выше">
                    ≥50
                </a>
            </li>
            <li class="toggle-menu__item">
                <a href="{{ route('hub', [ 'hub' => $hub->slug, 'top' => 'top100' ]) }}"
                    class="toggle-menu__item-link {{ $top === 'top100' ? 'toggle-menu__item-link_active' : '' }}"
                    rel="nofollow"
                    title="Все публикации с рейтингом 100 и выше">
                    ≥100
                </a>
            </li>
        </ul>
    </div>
</div>
