@extends('layouts.main')

@section('content')
<div class="layout__row layout__row_body">
    @include('hub.header')

    <div class="column-wrapper column-wrapper_tabs js-sticky-wrapper">
        <div class="content_left js-content_left">
            @include('hub.tabs')

            <div class="posts_list">
                @include('hub.posts')
                <div class="page__footer">
                    {{ $posts->links('hub.pagination') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
