<ul class="content-list shortcuts_items">
    @foreach ($posts as $post)
    <li class="content-list__item content-list__item_post shortcuts_item" id="post_{{ $post->id }}">
        <article class="post post_preview" lang="ru">

            <header class="post__meta">
                <a href="{{ $post->author->nickname }}" class="post__user-info user-info" title="Автор публикации">
                    <img src="{{ $post->author->avatar }}" width="24" height="24" class="user-info__image-pic user-info__image-pic_small">
                    <span class="user-info__nickname user-info__nickname_small">{{ $post->author->nickname }}</span>
                </a>
                <span class="post__time">{{ $post->created_date }}</span>
            </header>

            <h2 class="post__title">
                <a href="{{ route('post', [ 'post' => $post->id ]) }}" class="post__title_link">{{ $post->title }}</a>
            </h2>

            <ul class="post__hubs inline-list">
                @foreach ($post->hubs as $hub)
                <li class="inline-list__item inline-list__item_hub">
                    <a href="{{ route('hub', [ 'hub' => $hub->slug ]) }}"
                        class="inline-list__item-link hub-link "
                        rel="nofollow">
                        {{ $hub->title }},
                    </a>
                </li>
                @endforeach
            </ul>

            <ul class="post__marks inline-list">
                @foreach ($post->marks as $mark)
                <li class="inline-list__item inline-list__item_post-type">
                    <a href="#" class="post__type-label" title="Перейти в песочницу">{{ $mark->title }}</a>
                </li>
                @endforeach
            </ul>

            <div class="post__body post__body_crop ">

                <div class="post__text post__text-html  post__text_v1 ">
                    {{ $post->preview }}
                </div>

                <a class="btn btn_x-large btn_outline_blue post__habracut-btn" href="{{ route('post', [ 'post' => $post->id ]) }}/#habracut">Читать дальше →</a>
            </div>

            <footer class="post__footer">
                <ul class="post-stats  js-user_" data-post-type="publish_ugc_ru,h_91,h_260,h_359,f_develop" id="infopanel_post_{{ $post->id }}">

                    <li class="post-stats__item">
                        <div class="post-stats__result">
                            <span class="post-stats__result-icon">
                                <svg class="icon-svg_votes" width="10" height="16">
                                    <use xlink:href="/images/svg/common-svg-sprite.svg#counter-rating"></use>
                                </svg>
                            </span>
                            <span class="post-stats__result-counter voting-wjt__counter_{{ $post->rating >= 0 ? 'positive' : 'negative' }}"
                                title="Всего голосов {{ $post->votes_count }}: ↑{{ $post->positive_votes_count }} и ↓{{ $post->negative_votes_count }}">
                                {{ $post->rating }}
                            </span>
                        </div>
                    </li>

                    <li class="post-stats__item post-stats__item_bookmark">
                        <button type="button" class="btn bookmark-btn bookmark-btn_post " data-post-type="publish_ugc_ru,h_91,h_260,h_359,f_develop" data-type="2" data-id="{{ $post->id }}" data-action="add" title="Только зарегистрированные пользователи могут добавлять публикации в закладки" disabled="">
                            <span class="btn_inner">
                                <svg class="icon-svg_bookmark" width="10" height="16">
                                    <use xlink:href="/images/svg/common-svg-sprite.svg#book"></use>
                                </svg>
                                <span class="bookmark__counter js-favs_count" title="Количество пользователей, добавивших публикацию в закладки">
                                    {{ $post->bookmarks_count }}
                                </span>
                            </span>
                        </button>
                    </li>

                    <li class="post-stats__item post-stats__item_views">
                        <div class="post-stats__views" title="Количество просмотров">
                            <svg class="icon-svg_views-count" width="21" height="12">
                                <use xlink:href="/images/svg/common-svg-sprite.svg#eye"></use>
                            </svg>
                            <span class="post-stats__views-count">
                                {{ $post->views }}
                            </span>
                        </div>
                    </li>

                    <li class="post-stats__item post-stats__item_comments">
                        <a href="{{ route('post', [ 'post' => $post->id ]) }}/#comments" class="post-stats__comments-link" rel="nofollow">
                            <svg class="icon-svg_post-comments" width="16" height="16">
                                <use xlink:href="/images/svg/common-svg-sprite.svg#comment"></use>
                            </svg>
                            <span class="post-stats__comments-count" title="Читать комментарии">
                                {{ $post->comments_count }}
                            </span>
                        </a>
                    </li>

                </ul>
            </footer>
        </article>
    </li>
    @endforeach
</ul>
