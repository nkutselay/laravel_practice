import axios from 'axios';

console.log(window.axios);

class PostService {
    constructor() {
        this.client = axios.create({
            headers: {
            'Content-Type': 'application/json'
            },
            timeout: 30000,
            withCredentials: true
        });
    }

    async addVote(postId, value) {
        const { data } = await this.client.post(`/post/${postId}/vote`, {
            data: { value }
        });

        return data;
    }
}

export default new PostService();
