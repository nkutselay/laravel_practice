require('./bootstrap');

import PostVotes from './components/PostVotes.vue';

Vue.component('post-votes', PostVotes);

const app = new Vue({
    el: '#app'
});
